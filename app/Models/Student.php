<?php

namespace App\Models;

use Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;
    protected $fillable = [
        'id', 'first_name', 'last_name', 'gender', 'dob', 'school_name', 'university'
    ];

    public function getAgeAttribute()
    {
        return (date('Y') - date('Y', strtotime( $this->attributes['dob'] )));
    }
   
    public function getDateAttribute()
    {
        return date('d-m-Y',strtotime( $this->attributes['dob'] ));
    }
}

