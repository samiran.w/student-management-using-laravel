<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StudentRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'first_name'=>'required',
            'last_name'=>'required',
            'dob' => 'required|date_format:d-m-Y',
            'gender'=>'required|in:male,female,other',
            'school_name' => 'required',
            'university' => 'required',
        ];
    }
}
