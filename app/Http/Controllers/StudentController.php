<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\StudentRequest;
use Illuminate\Http\Request;

use App\Models\Student;

class StudentController extends Controller
{
    

    public function display()
    {
        $students= Student::all();  
        return view('display',['st'=>$students]);
    }

    
    

    public function create(StudentRequest $request)
    {  
        $student = new Student;
        $student->first_name = $request->first_name;
        $student->last_name = $request->last_name;
        $student->gender = $request->gender;
        $student->dob = date("Y-m-d",strtotime($request->dob));
        $student->school_name = $request->school_name;
        $student->university = $request->university;
        $student->save();
        return redirect('display');
    }

    public function delete($id)
    {
        $student = Student::find($id);
        $student->delete();
        return redirect('display');
    }

    public function edit($id)
    {
        $student = Student::find($id);
        // dd($student);
        return view('edit',['student'=> $student]);
    }

    public function update(StudentRequest $request)
    {
        $student = Student::find($request->id);
        $student->first_name = $request->first_name;
        $student->last_name = $request->last_name;
        $student->gender = $request->gender;
        $student->dob = date("Y-m-d",strtotime($request->dob));
        $student->school_name = $request->school_name;
        $student->university = $request->university;
        $student->save();
        return redirect('display');
    }
}
