<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StudentController;


Route::view('create','create');

Route::get('display',[StudentController::class,'display']);
Route::get('delete/{id}',[StudentController::class,'delete']);
Route::get('edit/{id}',[StudentController::class,'edit']);
Route::post('edit',[StudentController::class,'update']);
Route::post('create',[StudentController::class,'create']);

