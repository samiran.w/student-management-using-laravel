<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
    table, th, td {border:1px solid black;}
    table{border-collapse:collapse;}
    th{ font-size: 1.3em;}
    td,th{ padding : 10px 20px;}
    </style>
</head>
<body>
    <h1>Student Details</h1>
    <table>
        <tr>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Gender</th>
        <th>Age</th>
        <th>School Name</th>
        <th>University</th>
        <th colspan="2">Actions</th>
        </tr>
  @foreach($st as $student)
    <tr>
   
      <td>{{$student['first_name']}}</td>
      <td>{{$student['last_name']}}</td>
      <td>{{$student['gender']}}</td>
      <td>{{$student->age}}</td>
      <td>{{$student['school_name']}}</td>
      <td>{{$student['university']}}</td>
      <td><a href="{{"edit/".$student['id']}}">Edit</a></td>
      <td><a href={{"delete/".$student['id']}}>Delete</a></td>
    </tr>
  @endforeach
    </table>
</body>
</html>