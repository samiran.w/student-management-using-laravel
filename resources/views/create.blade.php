<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Student Management</title>
    <style>
        table{ border-collapse:collapse; }
        td,th{ padding : 2px 10px;}
        li{ color : red}
    </style>
</head>
<body>
    @if($errors->any())
        @foreach ($errors->all() as $err)
            <li>{{$err}}</li>
        @endforeach<br>
    @endif
        
    <form method="post" action="create" >
        @csrf
        <table>
            <tr><td><label for="first_name">Last Name :</label></td> <td><input type="text" name="first_name"></td></tr>
            <tr><td><label for="last_nam">Last Name :</label></td> <td><input type="text" name="last_name"></td></tr>
            <tr><td><label for="gender">Gender :</label></td> 
                <td><select name="gender" id="gender">
                    <option value="male">Male</option>
                    <option value="female">Female</option>
                    <option value="other">Other</option>
                  </select></td></tr>
            <tr><td><label for="dob">Birth Date :</label></td> <td><input type="text" placeholder="dd-mm-yy" name="dob"></td></tr>
            <tr><td><label for="school_name">School Name :</label></td> <td><input type="text" name="school_name"></td></tr>
            <tr><td><label for="university">Universiy :</label></td> <td><input type="text" name="university"></td></tr>
            <tr><td><input type="submit" value="Add Student"></tr>
        </table> 
    </form>
</body>
</html>