<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Student Management</title>
    <style>

    table{
        border-collapse:collapse;
    }
    td,th{
        padding : 2px 10px;
    }
    </style>
</head>
<body>
    <form method="post" action="/edit" >
        @csrf   
        <input type="hidden" name="id" value={{$student['id']}}>
        <table>
            <tr><td><label for="first_name">FirstName :</label></td> <td><input type="text" name="first_name" value="{{ $student['first_name'] }}"></td></tr>
            <tr><td><label for="last_name">Last Name :</label></td> <td><input type="text" name="last_name" value="{{ $student['last_name'] }}"></td></tr>
            <tr><td><label for="gender">Gender :</label></td> <td><input type="text" name="gender" value="{{ $student['gender'] }}"></td></tr>
            <tr><td><label for="dob">Birth Date :</label></td> <td><input type="text" placeholder="dd-mm-yy" name="dob" value="{{ $student->date     }}"></td></tr>
            <tr><td><label for="school_name">School Name :</label></td> <td><input type="text" name="school_name" value="{{ $student['school_name'] }}"></td></tr>
            <tr><td><label for="university">Universiy :</label></td> <td><input type="text" name="university" value="{{ $student['university'] }}"></td></tr>
            <tr><td><input type="submit" value="Edit Student"></tr>
        </table> 
    </form>
</body>
</html>